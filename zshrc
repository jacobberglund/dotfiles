# Comment
echo $SHELL
export EDITOR=/usr/bin/mvim
# export BROWSER=/Applications/Google Chrome.app

PATH=~/.composer/vendor/bin:$PATH

# Prompt
export PS1=$'%~\n\e[0;31m$ \e[0m '

export CLICOLOR=1
export TERM=xterm-256color

# Tab completion
autoload -U compinit
compinit
setopt completeinword

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

# setopts
setopt beep 		# removes error beep
setopt AUTO_CD		# cd into directory if exists

source ~/dotfiles/zsh-modules/aliases.zsh
source ~/dotfiles/zsh-modules/zsh-syntax-highlighting.zsh

# Colors

# zsh is able to auto-do some kungfoo
# depends on the SUFFIX :)
if [ ${ZSH_VERSION//\./} -ge 420 ]; then
  # open browser on urls
  _browser_fts=(htm html de org net com at cx nl se dk dk php)
  for ft in $_browser_fts ; do alias -s $ft=$BROWSER ; done

  _editor_fts=(cpp cxx cc c hh h inl asc txt TXT tex)
  for ft in $_editor_fts ; do alias -s $ft=$EDITOR ; done

  _image_fts=(jpg jpeg png gif mng tiff tif xpm)
  for ft in $_image_fts ; do alias -s $ft=$XIVIEWER; done

  _media_fts=(ape avi flv mkv mov mp3 mpeg mpg ogg ogm rm wav webm)
  for ft in $_media_fts ; do alias -s $ft=mplayer ; done

  #read documents
  alias -s pdf=acroread
  alias -s ps=gv
  alias -s dvi=xdvi
  alias -s chm=xchm
  alias -s djvu=djview

  #list whats inside packed file
  alias -s zip="unzip -l"
  alias -s rar="unrar l"
  alias -s tar="tar tf"
  alias -s tar.gz="echo "
  alias -s ace="unace l"
fi


# toggle iTerm Dock icon
# add this to your .bash_profile or .zshrc
function toggleiTerm() {
	pb='/usr/libexec/PlistBuddy'
	iTerm='/Applications/iTerm.app/Contents/Info.plist'
	
	echo "Do you wish to hide iTerm in Dock?"
	select ync in "Hide" "Show" "Cancel"; do
	    case $ync in
	        'Hide' )
	        	$pb -c "Add :LSUIElement bool true" $iTerm
	        	echo "relaunch iTerm to take effectives"
	        	break
	        	;;
	        'Show' )
	        	$pb -c "Delete :LSUIElement" $iTerm
	        	echo "run killall 'iTerm' to exit, and then relaunch it"
	        	break
	        	;;
		'Cancel' )
			break
			;;
	    esac
	done
}

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
