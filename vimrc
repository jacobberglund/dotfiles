" VIMRC by Jacob Berglund
" Suprised I actually got this far in VIM.

set nocompatible	" Sets this vimrc to not be compatible with another vimrc

" set background=dark

" -- Vim Plug to install plugins

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.vim/plugged')

	" General
	Plug 'terryma/vim-expand-region'
	Plug 'itchyny/lightline.vim'
	Plug 'nelsyeung/twig.vim'

	" Autocomplete & Snippets
	Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
	Plug 'Shougo/neosnippet'

	" Search
	Plug 'rking/ag.vim'
	Plug '/usr/local/opt/fzf' | Plug 'junegunn/fzf.vim'
		" Requires ZFZ to be installed with homebrew.

	" Colors
	Plug 'jacoborus/tender'

	" JS
	Plug 'othree/yajs.vim', { 'for': 'javascript' }

" CSS
Plug 'JulesWang/css.vim'
        \| Plug 'hail2u/vim-css3-syntax'
        \| Plug 'cakebaker/scss-syntax.vim'
" Plug 'othree/csscomplete.vim'
Plug 'mattn/emmet-vim'


	call plug#end()
" END PLUG

filetype plugin indent on
set omnifunc=syntaxcomplete#Complete

" -- Paste!
nnoremap <F2> :set invpaste paste?<CR>
" 					^ This lets us use pastemode in Insert
set pastetoggle=<F2>
"					^ Pastemode all over

" If you have vim >=8.0 or Neovim >= 0.1.5
if (has("termguicolors"))
 set termguicolors
endif

" For Neovim 0.1.3 and 0.1.4
let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" Making FZF respect .gitignore
let $FZF_DEFAULT_COMMAND = 'ag -g ""'

" Theme
syntax enable


" -- Performance
set lazyredraw
set ttyfast


" -- Command Line
set noshowcmd		" show incomplete commands (SLOW so off)
set noshowmode		" don't show -- INSERT -- in cmdline)


" -- Quality of life
set noerrorbells


" -- Editor settings
set relativenumber	" Show numbers relative to the line cursor is at
set number
set numberwidth=5	" Space between linenumbers and left side
set mouse=a			" Enable scrolling etc
set showmatch		" Show matching bracket
set noruler			" Don't show VIM ruler in the bottom right
set cursorline		" Enables cursorline. Duh.
set scrolloff=7		" How many rows offset when scrolling in a long document
set noautowrite		" Research this
set hidden			" 'hide' buffers when opening a new one: http://usevim.com/2012/10/19/vim101-set-hidden/
					" Also remembers undo after quitting

set foldcolumn=0

" VIM file explorer settings
let g:netrw_list_hide='\(^\|\s\s\)\zs\.\S\+'
let g:netrw_hide=1              " hide hidden files
let g:netrw_dirhistmax=100      " keep more history
let g:netrw_altfile=1           " last edited file '#'
let g:netrw_liststyle=0         " thin
let g:netrw_banner=0			" Hide banner
let g:netrw_alto=0              " open files on right
let g:netrw_winsize=40          " preview winsize
let g:netrw_preview=1           " open previews vertically
let g:netrw_use_errorwindow=0   " suppress error window

" Neat trick -- reveal already opened files from the quickfix window instead of opening new buffers
set switchbuf=useopen


" -- Swap/Backup/Undo
" Note -- : Double slash means create dir structure to mirror file's path

" -- Swap
set noswapfile 
set directory=~/.vim-tmp/swap//

" -- Backup
set backup			" Can you guess?
set backupdir=~/.vim-tmp/backup//

" -- Undo
set undofile
set undolevels=1000
set undoreload=10000
set undodir=~/.vim-tmp/undo//


" -- Wild
set browsedir=buffer                  " browse files in same dir as open file
set wildmenu                          " Enhanced command line completion.
set wildmode=list:longest,full        " Complete files using a menu AND list
set wildignorecase
" Ignore common files we're not editing/looking for in VIM.
set wildignore+=.git,.hg,.svn
set wildignore+=*/node_modules/* " Ignores folders
set wildignore+=*.aux,*.out,*.toc
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest,*.rbc,*.class
set wildignore+=*.ai,*.bmp,*.gif,*.ico,*.jpg,*.jpeg,*.png,*.psd,*.webp
set wildignore+=*.avi,*.m4a,*.mp3,*.oga,*.ogg,*.wav,*.webm
set wildignore+=*.eot,*.otf,*.ttf,*.woff
set wildignore+=*.doc,*.pdf
set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz
set wildignore+=*.swp,.lock,.DS_Store,._*


" -- Search related
set incsearch 		" Search as we type
set ignorecase 		" Ignore lower/uppercase in search
set hlsearch

" Tabbing 
set autoindent
set tabstop=4
set noexpandtab
set softtabstop=0
set shiftwidth=4
set smarttab
set nowrap
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2

" -- Leader
let mapleader=" "	" I like to use SPACE as my <leader>

" -- Key related settings:
set timeoutlen=500  " Lessen the lag after using the leader key

" -- Fixes
"	Remove highlighting when not needed
nnoremap <esc> :noh<esc> 

"	Enable going to next visually selected area with //
vnoremap // y/<C-R>"<CR>

"	Needed so that vim still understands escape sequences
nnoremap <esc>^[ <esc>^[

" -- Habits
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
imap jk <Esc>

" 	Leader+i to insert a character without entering insertmode
noremap <Leader>i i <Esc>r

" Expannd regins with space+v followed by additional v to expand
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

" Save file
nnoremap <Leader>w :w<CR>

" leader f to find
nnoremap <Leader>f /

" leader b to use FZF :Buffers
nnoremap <Leader>b :Buffers<CR>

" 	Use HJKL to navigate splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"	Resize windows
nnoremap <silent> <Leader>+ :exe "vertical resize " . (winwidth(0) * 3/2)<CR>
nnoremap <silent> <Leader>- :exe "vertical resize " . (winheight(0) * 2/3)<CR>

" -- Nice-to-have
"	 Bubbling lines
noremap <C-Up> ddkP
noremap <C-Down> ddp

" 	 Delete to end of row
noremap D d$ 

" 	 Bubble multiple lines
vmap <C-Up> xkP`[V`]
vmap <C-Down> xp`[V`]

" 	 Reselect after visualblock indent
vnoremap < <gv
vnoremap > >gv

" -- Leader keys
" map <leader>k :Vexplore<cr>
" map <leader>l :Explore<cr>
nmap <leader>n :tabe<cr>
nnoremap <leader>q :bd<cr>

"	 Edit and source vimrc
nnoremap <leader>ev :vs $MYVIMRC<cr>
nnoremap <leader>sv :so $MYVIMRC<cr>

"	 Save the buffers. Reopen where you were with Vim with 'vim -S
nnoremap <leader>W :mksession<CR> 

" -- Plugin related keys
imap <expr> <C-f> emmet#expandAbbrIntelligent("\<C-f>")

" -- Custom language related keys.

" Using a keyboard setting to disable dead keys. 
" Used the Ukelele.app to make it.

" Switch between regular keys and Swedish rebinds
fun! s:svenska()
    if &kmp=="swedish"
        set kmp=
    else
        set kmp=swedish
    endif
endfun

"	Set it from the get-go.
set keymap=swedish 

command! Svenska call s:svenska()
nnoremap <silent> <F5> :Svenska<CR>
inoremap <silent> <F5> <C-o>:Svenska<CR>

" 	 Set CSS syntax to SCSS. Mainly for PostCSS.
au BufRead,BufNewFile *.css set filetype=scss

" -- Looks
colorscheme tender
let g:lightline = {
      \ 'colorscheme': 'tender',
      \ }

" Line numbers and Cursors
hi LineNr ctermfg=Gray  guibg=NONE term=none 
hi VertSplit ctermbg=bg ctermfg=bg 
hi CursorLine cterm=none ctermbg=White

let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_SR = "\<Esc>]50;CursorShape=2\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"

" Lightline



" Deoplete Config
let g:deoplete#enable_at_startup = 1
let g:neosnippet#enable_snipmate_compatibility = 1
let g:deoplete#disable_auto_complete = 1
let g:neosnippet#snippets_directory='~/dotfiles/snippets/'


if has('conceal')
  set conceallevel=2 concealcursor=niv
endif

function! s:tab_complete()
  " is completion menu open? cycle to next item
  if pumvisible()
    return "\<c-n>"
  endif

  " is there a snippet that can be expanded?
  " is there a placholder inside the snippet that can be jumped to?
  if neosnippet#expandable_or_jumpable() 
    return "\<Plug>(neosnippet_expand_or_jump)"
  endif

  " if none of these match just use regular tab
  return "\<tab>"
endfunction


imap <silent><expr><TAB> <SID>tab_complete()

" UltiSnips config
"
" let g:UltiSnipsSnippetDirectories="~/dotfiles/snippets/"
"
" inoremap <silent><expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
" let g:UltiSnipsExpandTrigger="<C-x>"
" let g:UltiSnipsJumpForwardTrigger="<tab>"
" let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
"



