# Common commands
alias ..=' cd ..'
alias l='ls -AF'

# zsh related
alias zshrc='$EDITOR ~/.zshrc'
alias rezsh='source ~/.zshrc'
